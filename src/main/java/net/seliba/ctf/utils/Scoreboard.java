package net.seliba.ctf.utils;

import net.seliba.ctf.points.PointsManager;
import net.seliba.ctf.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

/**
 * Class which handles all operations
 * on the Scoreboard.
 */
public class Scoreboard {

    /**
     * Shows the (updated) Scoreboard to all Players
     * @param pointsManager Instance of the PointsManager
     */
    public static void setupScoreboard(PointsManager pointsManager) {

        //Iterate over all Players
        Bukkit.getOnlinePlayers().forEach(player -> {
            //Define Scoreboard and objective
            org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective objective = scoreboard.registerNewObjective("abc", "def", "§6Plagindefs");

            //Edit Scoreboard
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            objective.setDisplayName("§6Plagindefs");
            objective.getScore(" ").setScore(9);
            objective.getScore("§aWillkommen!").setScore(8);
            objective.getScore("  ").setScore(7);
            objective.getScore("§aPunkte:").setScore(6);
            objective.getScore("§1Blau: §a" + pointsManager.getTeamPoints(Team.BLUE)).setScore(5);
            objective.getScore("§cRot: §a" + pointsManager.getTeamPoints(Team.RED)).setScore(4);
            objective.getScore("   ").setScore(3);
            objective.getScore("§aDeine Punkte:").setScore(2);
            objective.getScore("§6" + pointsManager.getPlayerPoints(player)).setScore(1);

            //Show Scoreboard to Player
            player.setScoreboard(scoreboard);
        });

    }

}
