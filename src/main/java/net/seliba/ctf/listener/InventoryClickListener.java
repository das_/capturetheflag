package net.seliba.ctf.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Class which handles all Inventory clicks.
 */
public class InventoryClickListener implements Listener {

    /**
     * Executed when a Player clicks in an Inventory.
     * Used to disable any Inventory movement.
     * @param event The InventoryClickEvent, provided by Spigot
     */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        //Define the Player
        Player player = (Player) event.getWhoClicked();

        //Check if the Player is not in Creative mode
        if(player.getGameMode() != GameMode.CREATIVE) {
            //Cancel the event
            event.setCancelled(true);
        }

    }

}
