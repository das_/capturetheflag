package net.seliba.ctf.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * Class which handles weather changes.
 */
public class WeatherChangeListener implements Listener {

    /**
     * Executed when the weather changes.
     * Used to cancel the weather change.
     * @param event The WeatherChangeEvent, provided by Spigot
     */
    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {

        //Always cancel the event
        event.setCancelled(true);

    }

}
