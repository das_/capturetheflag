package net.seliba.ctf.listener;

import net.seliba.ctf.points.PointsManager;
import net.seliba.ctf.teams.Team;
import net.seliba.ctf.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Class which handles Player quits.
 */
public class PlayerQuitListener implements Listener {

    private TeamManager teamManager;
    private PointsManager pointsManager;

    /**
     * The default constructor, used to inject dependencies
     * into the class.
     * @param teamManager The instance of the TeamManager
     * @param pointsManager The instance of the PointsManager
     */
    public PlayerQuitListener(TeamManager teamManager, PointsManager pointsManager) {

        this.teamManager = teamManager;
        this.pointsManager = pointsManager;

    }

    /**
     * Executed when a Player leaves the server.
     * Used to remove him from his team and other
     * various things.
     * @param event The PlayerQuitEvent, provided by Spigot
     */
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        //Define the Player
        Player player = event.getPlayer();

        //Change message
        event.setQuitMessage("§8[§c-§8] §7" + player.getName());

        //Remove the Player from the team
        teamManager.leave(player);

        //Reset the Player points
        pointsManager.resetPlayerPoints(player);

        //Reset flag if the Player owned one
        if (player.getInventory().contains(Material.BLUE_BANNER)) {
            teamManager.resetFlag(Team.BLUE);
            Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.BLUE.getName() + " §awurde zurückgesetzt!");
        } else if(player.getInventory().contains(Material.RED_BANNER)) {
            teamManager.resetFlag(Team.RED);
            Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.RED.getName() + " §awurde zurückgesetzt!");
        }

        //Clear the Inventory of the Player
        player.getInventory().clear();

    }

}
