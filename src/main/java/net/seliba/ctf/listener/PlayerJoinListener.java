package net.seliba.ctf.listener;

import net.seliba.ctf.points.PointsManager;
import net.seliba.ctf.teams.Team;
import net.seliba.ctf.teams.TeamManager;
import net.seliba.ctf.utils.Scoreboard;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Class which handles Player joins.
 */
public class PlayerJoinListener implements Listener {

    private TeamManager teamManager;
    private PointsManager pointsManager;

    /**
     * The default constructor, used to inject dependencies
     * into the class.
     * @param teamManager The instance of the TeamManager
     * @param pointsManager The instance of the PointsManager
     */
    public PlayerJoinListener(TeamManager teamManager, PointsManager pointsManager) {

        this.teamManager = teamManager;
        this.pointsManager = pointsManager;

    }

    /**
     * Executed when a Player joins the server.
     * Used to assign the Player to a team,
     * give him a Scoreboard and reset him.
     * @param event The PlayerJoinEvent, provided by Spigot
     */
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        //Define the Player
        Player player = event.getPlayer();

        //Add to team
        teamManager.joinFair(player);

        //Define the team of the Player
        Team team = teamManager.getTeam(player);

        //Reset the Player
        teamManager.resetPlayer(player);

        //Modify message
        event.setJoinMessage("§8[§a+§8] §7" + player.getName());

        //Set up the Scoreboard
        Scoreboard.setupScoreboard(pointsManager);

        //Send message to Player
        player.sendMessage("§6CTF §7» §aDu bist nun in Team " + team.getName());

    }

}
