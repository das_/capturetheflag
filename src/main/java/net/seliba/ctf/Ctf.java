package net.seliba.ctf;

import net.seliba.ctf.listener.*;
import net.seliba.ctf.points.PointsManager;
import net.seliba.ctf.teams.Team;
import net.seliba.ctf.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.WorldCreator;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The Main class which enables the Plugin
 * and manages everything.
 */
public class Ctf extends JavaPlugin {

    private TeamManager teamManager;
    private PointsManager pointsManager;

    /**
     * Called when the Plugin starts.
     * Used to create the default World and
     * register the Listeners.
     */
    @Override
    public void onEnable() {

        //Send startup message
        Bukkit.getConsoleSender().sendMessage("[CTF] Gestartet!");

        //Define the TeamManager and the PointsManager
        this.teamManager = new TeamManager();
        this.pointsManager = new PointsManager(teamManager);

        //Load world and set default settings
        Bukkit.createWorld(new WorldCreator("CTF"));
        removeDaylighCycle();
        removeEntities();

        //Register all Listeners
        registerListener();

        //Start min Y listener
        startLocationChecker();

    }

    /**
     * Called when the Plugin stops.
     * Used the reset the flags of the teams.
     */
    @Override
    public void onDisable() {

        //Reset the flags
        teamManager.resetFlag(Team.BLUE);
        teamManager.resetFlag(Team.RED);

    }

    /**
     * Registers all Listener which are needed
     * for the Plugin to work.
     */
    private void registerListener() {

        //Define the PluginManager which is used to register the Listener
        PluginManager pluginManager = Bukkit.getPluginManager();

        //Register all Listeners
        pluginManager.registerEvents(new BlockEditListener(), this);
        pluginManager.registerEvents(new EntitySpawnListener(), this);
        pluginManager.registerEvents(new FoodLevelChangeListener(), this);
        pluginManager.registerEvents(new InventoryClickListener(), this);
        pluginManager.registerEvents(new PlayerDamageByEntityListener(teamManager), this);
        pluginManager.registerEvents(new PlayerDeathListener(teamManager, pointsManager), this);
        pluginManager.registerEvents(new PlayerDropListener(), this);
        pluginManager.registerEvents(new PlayerInteractListener(teamManager, pointsManager), this);
        pluginManager.registerEvents(new PlayerJoinListener(teamManager, pointsManager), this);
        pluginManager.registerEvents(new PlayerQuitListener(teamManager, pointsManager), this);
        pluginManager.registerEvents(new WeatherChangeListener(), this);

    }

    /**
     * Removes the daylight cycle from the CTF-Map.
     */
    private void removeDaylighCycle() {

        try {
            Bukkit.getWorld("CTF").setTime(1000L);
            Bukkit.getWorld("CTF").setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        } catch (NullPointerException exception) {
            //Do nothing
        }

    }

    /**
     * Removes all entities from the CTF-Map.
     */
    private void removeEntities() {

        try {
            Bukkit.getWorld("CTF").getEntities().forEach(entity -> {
                if(entity.getType() != EntityType.PLAYER && entity.getType() != EntityType.ARMOR_STAND) {
                    entity.remove();
                }
            });
        } catch (NullPointerException exception) {
            //Do nothing
        }

    }

    private void startLocationChecker() {

        //Create new ScheduledExecutorService
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        //Kill the player if he is under the Map
        executorService.scheduleWithFixedDelay(
                () ->
                    Bukkit.getOnlinePlayers().stream()
                            .filter(player -> player.getLocation().getY() == -1)
                            .forEach(player -> player.setHealth(0)),
                0L,
                1,
                TimeUnit.SECONDS
        );

    }

}
