package net.seliba.ctf.points;

import net.seliba.ctf.teams.Team;
import net.seliba.ctf.teams.TeamManager;
import net.seliba.ctf.utils.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PointsManager {

    private TeamManager teamManager;
    private HashMap<Player, Integer> playerPoints;
    private final int MAX_POINTS = 100;

    public PointsManager(TeamManager teamManager) {

        this.teamManager = teamManager;
        this.playerPoints = new HashMap<>();

    }

    /**
     * Add points to a Player
     * @param player The Player who should get the points
     * @param pointReason The reason why the points should be added
     */
    public void addPoints(Player player, PointReason pointReason) {

        addPoints(player, pointReason.getPoints());

    }

    /**
     * Add points to a Player.
     * Only needed for internal purposes.
     * @param player The Player who should get the points
     * @param coinAmount The amount of coins which the Player should get
     */
    private void addPoints(Player player, int coinAmount) {

        //Get the current amount of points
        Integer currentPlayerCoins = playerPoints.get(player);

        //Check if the Player has currently points
        if(currentPlayerCoins == null) {
            currentPlayerCoins = 0;
        }

        //Add coins
        currentPlayerCoins += coinAmount;

        //Save coins to Map
        playerPoints.put(player, currentPlayerCoins);

        //Change Scoreboard of every Player
        Scoreboard.setupScoreboard(this);

        //Check if the max. point amount is reached
        if(getTeamPoints(teamManager.getTeam(player)) >= MAX_POINTS) {
            //Send messages
            Bukkit.broadcastMessage("§6CTF §7» §aDas Spiel ist zuende!");
            Bukkit.broadcastMessage("§6CTF §7» §aGewinnerteam: Team " + teamManager.getTeam(player).getName());

            //Reset the flags
            teamManager.resetFlag(Team.BLUE);
            teamManager.resetFlag(Team.RED);

            //Reset every Player
            Bukkit.getOnlinePlayers().forEach(teamManager::resetPlayer);

            //Reset all points
            resetTeamPoints();
        }

    }

    /**
     * Resets the points of every Player on the server.
     */
    private void resetTeamPoints() {

        //Clears all points
        playerPoints.clear();

        //Change scoreboard
        Bukkit.getOnlinePlayers().forEach(onlinePlayer ->
                Scoreboard.setupScoreboard(this)
        );

    }

    /**
     * Resets the points of a Player
     * @param player The Player whom points should be reseted
     */
    public void resetPlayerPoints(Player player) {

        playerPoints.remove(player);

    }

    /**
     * Returns the sum of points from any team member
     * in the team.
     * @param team The team whom points should be accessed
     * @return The amount of points the team has
     */
    public int getTeamPoints(Team team) {

        //Initialize default value for points
        int points = 0;

        //Iterate over all online Players
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            //Check if the Player is in the provided team
            if (teamManager.getTeam(onlinePlayer) == team) {
                //Add the Player coin amount to the team point amount
                points += getPlayerPoints(onlinePlayer);
            }
        }

        //Return the points of the team
        return points;

    }

    /**
     *
     * @param player The Player whom points should be accessed
     * @return The amount of points the Player has
     */
    public int getPlayerPoints(Player player) {

        return playerPoints.get(player) == null ? 0 : playerPoints.get(player);

    }

}
