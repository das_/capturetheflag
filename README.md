**CaptureTheFlag**

A simple, non-configurable Capture the Flag plugin for Spigot servers written in Java. 

Created by Seliba during a "Minecraft Server in 48 hours" contest in ~5 hours (including bugfixes & tests).